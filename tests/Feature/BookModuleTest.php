<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use App\Models\Book;
use Illuminate\Http\Response;
use App\Http\Controllers\BaseController;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BookModuleTest extends TestCase
{
    use WithFaker;
    protected $connectionsToTransact = ['sqlite'];
    /** @test */
    public function test_a_books_can_be_retrieved()
    {
        if (!User::first()) {
            factory(User::class, 1)->create();
        }
        if (!Book::first()) {
            factory(Book::class, 1)->create();
        }
        $book = Book::first();
        $params = [
            'user_id' => $book->user_id
        ];
        $this->json('get', route("books.index", $params))->assertStatus(Response::HTTP_OK)->assertJsonStructure(
            [
                "success",
                'data' => [
                    '*' => [
                        'id',
                        'title',
                        'description'
                    ]
                ]
            ]
        );
    }

    /** @test */
    public function test_a_book_can_be_created()
    {
        if (!User::first()) {
            factory(User::class, 1)->create();
        }
        $payload = [
            'title' => $this->faker->title,
            'description' => $this->faker->text,
            'user_id' => User::first()->id,
            'price' => rand(50, 200)
        ];
        $this->json('post', route("books.store"), $payload)
            ->assertStatus(Response::HTTP_CREATED)
            ->assertJsonStructure(
                [
                    'data' => [
                        'id',
                        'title',
                        'description',
                        'price',
                    ]
                ]
            );
        $this->assertDatabaseHas('books', $payload);
    }

    /** @test */
    public function test_a_book_can_be_retrieved()
    {
        if (!User::first()) {
            factory(User::class, 1)->create();
        }
        $payload = [
            'title' => $this->faker->title,
            'description' => $this->faker->text,
            'user_id' => User::first()->id,
            'price' => rand(50, 200)
        ];
        $book = Book::create(
            $payload
        );
        $this->withoutExceptionHandling();

        $this->json('get', route("books.show", $book->id))->assertStatus(Response::HTTP_OK)->assertExactJson(
            [
                "success" => true,
                'data' => [
                    'id' => $book->id,
                    'title' => $book->title,
                    'description' => $book->description,
                    'price' => $book->price,
                ]
            ]
        );
    }

    /** @test */
    public function test_a_book_can_be_destroyed()
    {
        if (!User::first()) {
            factory(User::class, 1)->create();
        }
        $payload = [
            'title' => $this->faker->title,
            'description' => $this->faker->text,
            'user_id' => User::first()->id,
            'price' => rand(50, 200)
        ];
        $book = Book::create(
            $payload
        );

        $this->json('delete', route("books.destroy", $book->id))
            ->assertOk();
        $this->assertSoftDeleted('books', $book->toArray());
    }

    public function test_a_book_can_be_updated()
    {
        $this->withoutExceptionHandling();

        if (!User::first()) {
            factory(User::class, 1)->create();
        }
        $payload = [
            'title' => $this->faker->title,
            'description' => $this->faker->text,
            'user_id' => User::first()->id,
            'price' => rand(50, 200)
        ];
        $book = Book::create(
            $payload
        );

        $_payload = [
            'title' => $this->faker->title,
            'description' => $this->faker->text,
            'price' => rand(50, 200)
        ];

        $this->json('put', route("books.update", $book->id), $_payload)
            ->assertStatus(Response::HTTP_OK)
            ->assertExactJson(
                [
                    'success' => true,
                    'data' => [
                        'id' => $book->id,
                        'title' => $_payload['title'],
                        'description' => $_payload['description'],
                        'price' => $_payload['price'],
                    ],
                    'message' => BaseController::UPDATED_SUCCESS_MESSAGE,

                ]
            );
    }

    /** @test */
    public function test_list_of_books_can_be_retrieved()
    {
        $this->json('get', route("books.list"))->assertStatus(Response::HTTP_OK)->assertJsonStructure(
            [
                "success",
                'data' => [
                    '*' => [
                        'id',
                        'title',
                        'description'
                    ]
                ],
            ]
        );
    }

    /** @test */
    public function test_filtered_list_of_books_can_be_retrieved()
    {
        $params = [
            'sort' => [
                'column' => 'title',
                'direction' => 'asc'
            ],
            'perPage' => 15
        ];
        $this->json('get', route("books.list.paginate", $params))->assertStatus(Response::HTTP_OK)->assertJsonStructure(
            [
                "success",
                'data' => [
                    '*' => [
                        'id',
                        'title',
                        'description'
                    ]
                ],
                'pagination' => [
                    'totalPages',
                    'page',
                    'perPage'
                ]
            ]
        );
    }

}