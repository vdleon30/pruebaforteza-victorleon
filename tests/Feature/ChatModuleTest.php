<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use App\Models\Book;
use App\Models\Chat;
use Illuminate\Http\Response;
use Illuminate\Http\UploadedFile;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ChatModuleTest extends TestCase
{
    protected $connectionsToTransact = ['sqlite'];
    use WithFaker;

    /** @test */
    public function test_a_chat_can_be_created()
    {
        if (!User::count() < 2) {
            factory(User::class, 2)->create();
        }
        if (!Book::first()) {
            factory(Book::class, 1)->create();
        }
        $book = Book::first();
        $user = User::where("id", "<>", $book->id)->first();
        $payload = [
            'user_id' => $user->id,
            'book_id' => $book->id,
        ];
        $this->json('post', route("chats.start"), $payload)
            ->assertStatus(Response::HTTP_CREATED)
            ->assertJsonStructure(
                [
                    'data' => [
                        'id',
                        'user',
                        'author',
                        'book',
                        'status',
                        'totalMessage'
                    ]
                ]
            );
        $this->assertDatabaseHas('chats', $payload);
    }

    /** @test */
    public function test_a_user_can_be_send_message()
    {
        $chat = Chat::first();
        if (!$chat) {
            if (!User::count() < 2) {
                factory(User::class, 2)->create();
            }
            if (!Book::first()) {
                factory(Book::class, 1)->create();
            }
            $book = Book::first();
            $user = User::where("id", "<>", $book->id)->first();
            $chat = Chat::create([
                'user_id' => $user->id,
                'book_id' => $book->id,
                'author_id' => $book->user_id,
            ]);

        }
        $payload = [
            "chat_id" => $chat->id,
            "user_id" => $chat->user_id,
            "body" => "Este es el mensaje del Usuario"
        ];

        $this->json('post', route("chats.messages.store"), $payload)
            ->assertStatus(Response::HTTP_CREATED)->assertJsonStructure(
                [

                    "success",
                    'data' => [
                        'id',
                        'status',
                        'body',
                        'date',
                        'file',
                        'reciviedBy' => [
                            'id',
                            'name',
                            'email',
                        ],
                        'sendBy' => [
                            'id',
                            'name',
                            'email',
                        ],
                    ],
                    'message'
                ]
            );

    }
    /** @test */

    public function test_a_author_can_be_send_message()
    {
        $chat = Chat::first();
        if (!$chat) {
            if (!User::count() < 2) {
                factory(User::class, 2)->create();
            }
            if (!Book::first()) {
                factory(Book::class, 1)->create();
            }
            $book = Book::first();
            $user = User::where("id", "<>", $book->id)->first();
            $chat = Chat::create([
                'user_id' => $user->id,
                'book_id' => $book->id,
                'author_id' => $book->user_id,
            ]);

        }
        $payload = [
            "chat_id" => $chat->id,
            "user_id" => $chat->author_id,
            "body" => "Este es el mensaje del Autor"
        ];

        $this->json('post', route("chats.messages.store"), $payload)
            ->assertStatus(Response::HTTP_CREATED)->assertJsonStructure(
                [
                    "success",
                    'data' => [
                        'id',
                        'status',
                        'body',
                        'date',
                        'file',
                        'reciviedBy' => [
                            'id',
                            'name',
                            'email',
                        ],
                        'sendBy' => [
                            'id',
                            'name',
                            'email',
                        ],
                    ],
                    'message'
                ]
            );
    }

    /** @test */
    public function test_a_chat_can_be_retrieved()
    {
        $chat = Chat::first();
        if (!$chat) {
            if (!User::count() < 2) {
                factory(User::class, 2)->create();
            }
            if (!Book::first()) {
                factory(Book::class, 1)->create();
            }
            $book = Book::first();
            $user = User::where("id", "<>", $book->id)->first();
            $chat = Chat::create([
                'user_id' => $user->id,
                'book_id' => $book->id,
                'author_id' => $book->user_id,
            ]);

        }
        $params = [
            'chat_id' => $chat->id,
        ];

        $this->json('get', route("chats.get", $params))->assertStatus(Response::HTTP_OK)->assertExactJson(
            [
                "success" => true,
                'data' => [
                    'id' => $chat->id,
                    'user' => [
                        'id' => $chat->user->id,
                        'name' => $chat->user->name,
                        'email' => $chat->user->email,
                    ],
                    'author' => [
                        'id' => $chat->author->id,
                        'name' => $chat->author->name,
                        'email' => $chat->author->email,
                    ],
                    'book' => [
                        'id' => $chat->book->id,
                        'title' => $chat->book->title,
                        'description' => $chat->book->description,
                        'price' => $chat->book->price
                    ],
                    'status' => $chat->status,
                    'totalMessage' => $chat->messages->count(),
                    'date' => $chat->created_at
                ],
            ]
        );
    }
    /** @test */
    public function test_messages_of_chat_can_be_retrieved()
    {
        $chat = Chat::first();
        if (!$chat) {
            if (!User::count() < 2) {
                factory(User::class, 2)->create();
            }
            if (!Book::first()) {
                factory(Book::class, 1)->create();
            }
            $book = Book::first();
            $user = User::where("id", "<>", $book->id)->first();
            $chat = Chat::create([
                'user_id' => $user->id,
                'book_id' => $book->id,
                'author_id' => $book->user_id,
            ]);

        }
        $params = [
            'chat_id' => $chat->id,
        ];

        $this->json('get', route("chats.messages.get", $params))->assertStatus(Response::HTTP_OK)->assertJsonStructure(
            [
                "success",
                'data' => [
                    '*' => [
                        'id',
                        'status',
                        'sendBy',
                        'reciviedBy',
                        'date',
                        'file',
                        'body'
                    ]
                ],
                'pagination' => [
                    'totalPages',
                    'page',
                    'perPage'
                ]
            ]
        );
    }

    /** @test */
    public function test_a_messages_can_be_send_with_file()
    {
        $chat = Chat::first();
        if (!$chat) {
            if (!User::count() < 2) {
                factory(User::class, 2)->create();
            }
            if (!Book::first()) {
                factory(Book::class, 1)->create();
            }
            $book = Book::first();
            $user = User::where("id", "<>", $book->id)->first();
            $chat = Chat::create([
                'user_id' => $user->id,
                'book_id' => $book->id,
                'author_id' => $book->user_id,
            ]);

        }

        $file = UploadedFile::fake()->image('image.jpg');

        $payload = [
            "chat_id" => $chat->id,
            "user_id" => $chat->author_id,
            "body" => "Este es el mensaje del Usuario con archivo",
            "file" => $file
        ];

        $this->json('post', route("chats.messages.store"), $payload)
            ->assertStatus(Response::HTTP_CREATED)->assertJsonStructure(
                [
                    "success",
                    'data' => [
                        'id',
                        'status',
                        'body',
                        'date',
                        'file',
                        'reciviedBy' => [
                            'id',
                            'name',
                            'email',
                        ],
                        'sendBy' => [
                            'id',
                            'name',
                            'email',
                        ],
                    ],
                    'message'
                ]
            );

    }
}