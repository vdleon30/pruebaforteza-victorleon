<?php

namespace App\Providers;

use App\Repositories\BookRepository;
use App\Repositories\ChatRepository;
use Illuminate\Support\ServiceProvider;
use App\Interfaces\BookRepositoryInterface;
use App\Interfaces\ChatRepositoryInterface;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(BookRepositoryInterface::class, BookRepository::class);
        $this->app->bind(ChatRepositoryInterface::class, ChatRepository::class);

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}