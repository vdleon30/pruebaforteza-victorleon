<?php

namespace App\Repositories;

use App\Models\Book;
use Illuminate\Http\Request;
use App\Repositories\BaseRepository;
use App\Interfaces\BookRepositoryInterface;




class BookRepository extends BaseRepository implements BookRepositoryInterface
{
    public function getModel()
    {
        return new Book();
    }

    public function findFail($id)
    {
        return $this->getModel()->findOrFail($id);
    }

    public function getAll()
    {
        return $this->getModel()->get();
    }
    public function getAllByAuthor(int $user_id)
    {
        return $this->getModel()->whereUserId($user_id)->get();
    }

    public function store(Request $request)
    {
        return $this->getModel()->create($request->toArray());
    }

    public function modify(Request $request, Book $object)
    {
        $object->fill($request->toArray());
        $object->save();
        return $object;
    }

    public function destroy(Book $object)
    {
        $object->delete();
    }

    public function search(?string $search, ?array $sort, ?int $perPage = 10)
    {
        $model = $this->getModel();
        if(!empty($search)){
            $model = $model->where("title", '%like%', $search);
        }
        return $model->orderBy($sort["column"] ?? 'id', $sort["direction"] ?? 'asc')->paginate($perPage);
    }
}