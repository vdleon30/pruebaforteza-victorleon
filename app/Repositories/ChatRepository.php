<?php

namespace App\Repositories;

use App\Models\Book;
use App\Models\Chat;
use Illuminate\Http\Request;
use App\Repositories\BaseRepository;
use App\Interfaces\ChatRepositoryInterface;




class ChatRepository extends BaseRepository implements ChatRepositoryInterface
{
    public function getModel()
    {
        return new Chat();
    }

    public function findFail($id)
    {
        return $this->getModel()->findOrFail($id);
    }

    public function start(Request $request)
    {
        $data = $request->toArray();
        if(!$this->getModel()->where("book_id", $data["book_id"])->where("user_id", $data["user_id"])->first()){
            $data["author_id"] = Book::find($data["book_id"])->user_id;
            return $this->getModel()->create($data);
        }
        return $this->getModel()->where("book_id", $data["book_id"])->where("user_id", $data["user_id"])->first();

    }

    public function storeMessage(Request $request)
    {
        $object = $this->findFail($request->chat_id);
        $message = $object->messages()->create([
            "send_id" => $request->user_id,
            "recivied_id" => $request->user_id == $object->user_id ? $object->author_id : $object->user_id,
            "body" => $request->body
        ]);

        if ($request->file) {
            $file_name = $object->generateFileName($request->file);
            $request->file->storeAs('', $file_name, 'uploads');
            $message->update([
                "file_name" => $file_name
            ]);
        }
        return $message;
    }

    public function getMessage(Request $request)
    {
        $object = $this->findFail($request->chat_id);
        return $object->messages()->orderBy("created_at", "desc")->paginate(15);
    }
}