<?php
namespace App\Repositories;

use App\User;

abstract class BaseRepository
{
    protected $filters = [];
    protected $columns = [];

    abstract public function getModel();
}