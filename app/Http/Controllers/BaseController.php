<?php
namespace App\Http\Controllers;

use Illuminate\Http\Response;
use App\Http\Controllers\Controller as Controller;

class BaseController extends Controller
{
    const UPDATED_SUCCESS_MESSAGE = "Actualizado Exitosamente";
    const STORE_SUCCESS_MESSAGE = "Creado Exitosamente";
    const DESTROY_SUCCESS_MESSAGE = "Eliminado Exitosamente";
    const START_SUCCESS_MESSAGE = "Iniciado Exitosamente";
    const SEND_SUCCESS_MESSAGE = "Enviado Exitosamente";
    /**
     * success response method.
     *
     * @return \Illuminate\Http\JsonResponse
     */

    public function sendResponse($result, $message = false, $code = Response::HTTP_OK, ?object $collection = null)
    {
        $response = [
            'success' => true
        ];
        if(!empty($result)){
            $response["data"] = $result;
        }
        if(!empty($message)){
            $response["message"] = $message;
        }
        if(!empty($collection)){
            $response["pagination"] = [
                'totalPages' => $collection->lastPage(),
                'page' => $collection->currentPage(),
                'perPage' => $collection->perPage(),
                'count' => $collection->count(),
                'from' => $collection->firstItem(),
                'to' => $collection->lastItem(),
                'total' => $collection->total(),
            ];
        }
        return response()->json($response, $code);
    }

    /**
     * return error response.
     *
     * @return \Illuminate\Http\JsonResponse
     */

    public function sendError($error = "Ha ocurrido un error, Vuelva a intentar", $errorMessages = [], $code = 404)
    {
        $response = [
            'success' => false,
            'message' => $error,
        ];

        if (!empty($errorMessages)) {
            $response['data'] = $errorMessages;
        }
       
        return response()->json($response, $code);
    }
}
