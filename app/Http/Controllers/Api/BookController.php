<?php

namespace App\Http\Controllers\Api;

use App\Models\Book;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use App\Http\Resources\BookResource;
use App\Repositories\BookRepository;
use App\Http\Controllers\BaseController;

class BookController extends BaseController
{
    private $repository;

    public function __construct(BookRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @api {get} api/books Lista de libros/publicaciones
     * @apiGroup      Libros Crud
     * @apiName       books.index
     *  @apiParam      {Integer} user_id autor de los libros
     *  @apiSuccess    {Boolean} success  indica si todo se ejecuto correctamente
     *  @apiSuccess    {Object[]} data  contiene la informacion los libros
     *  @apiSuccess    {Integer} data.id  id del libro recien creado
     * @apiSuccess      {String} data.title titulo del libro
     *  @apiSuccess      {Text} data.description descripcion del libro
     *  @apiSuccess      {Double} data.price precio del libro
     */
    public function index(Request $request)
    {
        try {
            $books = $this->repository->getAllByAuthor($request->user_id);
            return $this->sendResponse(BookResource::collection($books), false, Response::HTTP_OK);
        } catch (\Exception $e) {
            Log::info("Ha ocurrido un error, Controller: Api\BookController@index - Error: {$e->getMessage()} - Line: {$e->getLine()}");
            return $this->sendError();
        }
    }

    /**
     * @api {post} api/books Guardar libro/publicacion dado un author
     * @apiGroup      Libros Crud
     * @apiName       books.store
     *  @apiParam      {String} title titulo del libro
     *  @apiParam      {Text} description descripcion del libro
     *  @apiParam      {Integer} user_id autor del libro
     *  @apiParam      {Double} price precio del libro
     * @apiSuccess    {Boolean} success  indica si todo se ejecuto correctamente
     *  @apiSuccess    {Object} data  contiene la informacion del libro guardado
     * @apiSuccess      {String} data.title titulo del libro
     *  @apiSuccess      {Text} data.description descripcion del libro
     *  @apiSuccess      {Integer} data.user_id autor del libro
     *  @apiSuccess      {Double} data.price precio del libro
     * @apiSuccess    {String} message mensaje alusivo a la acción realizada
     */
    public function store(Request $request)
    {
        try {
            $book = $this->repository->store($request);
            return $this->sendResponse(new BookResource($book), self::STORE_SUCCESS_MESSAGE, Response::HTTP_CREATED);
        } catch (\Exception $e) {
            Log::info("Ha ocurrido un error, Controller: Api\BookController@store - Error: {$e->getMessage()} - Line: {$e->getLine()}");
            return $this->sendError();
        }
    }

     /**
     * @api {get} api/books/:id Ver libro/publicacion
     * @apiGroup      Libros Crud
     * @apiName       books.show
     * @apiSuccess    {Boolean} success  indica si todo se ejecuto correctamente
     *  @apiSuccess    {Object} data  contiene la informacion del libro editado
     * @apiSuccess      {String} data.title titulo del libro
     *  @apiSuccess      {Text} data.description descripcion del libro
     *  @apiSuccess      {Integer} data.user_id autor del libro
     *  @apiSuccess      {Double} data.price precio del libro
     */
    public function show($id)
    {
        try {
            $book = $this->repository->findFail($id);
            return $this->sendResponse(new BookResource($book), false);
        } catch (\Exception $e) {
            Log::info("Ha ocurrido un error, Controller: Api\BookController@show - Error: {$e->getMessage()} - Line: {$e->getLine()}");
            return $this->sendError();
        }
    }

    /**
     * @api {put} api/books/:id Actualizar libro/publicacion de un author
     * @apiGroup      Libros Crud
     * @apiName       books.update
     * @apiParam      {String} title titulo del libro
     *  @apiParam      {Text} description descripcion del libro
     *  @apiParam      {Integer} user_id autor del libro
     *  @apiParam      {Double} price precio del libro
     * @apiSuccess    {Boolean} success  indica si todo se ejecuto correctamente
     *  @apiSuccess    {Object} data  contiene la informacion del libro editado
     * @apiSuccess      {String} data.title titulo del libro
     *  @apiSuccess      {Text} data.description descripcion del libro
     *  @apiSuccess      {Integer} data.user_id autor del libro
     *  @apiSuccess      {Double} data.price precio del libro
     * @apiSuccess    {String} message mensaje alusivo a la acción realizada
     */
    public function update(Request $request, $id)
    {
        try {
            $book = $this->repository->findFail($id);
            $book = $this->repository->modify($request, $book);
            return $this->sendResponse(new BookResource($book), self::UPDATED_SUCCESS_MESSAGE);
        } catch (\Exception $e) {
            Log::info("Ha ocurrido un error, Controller: Api\BookController@store - Error: {$e->getMessage()} - Line: {$e->getLine()}");
            return $this->sendError();
        }
    }

     /**
     * @api {delete} api/books/:id Eliminar libro/publicacion de un author
     * @apiGroup      Libros Crud
     * @apiName       books.destroy
     * @apiSuccess    {Boolean} success  indica si todo se ejecuto correctamente
     * @apiSuccess    {String} message mensaje alusivo a la acción realizada
     */
    public function destroy($id)
    {
        try {
            $book = $this->repository->findFail($id);
            $this->repository->destroy($book);
            return $this->sendResponse(null, self::DESTROY_SUCCESS_MESSAGE, Response::HTTP_OK);
        } catch (\Exception $e) {
            Log::info("Ha ocurrido un error, Controller: Api\BookController@destroy - Error: {$e->getMessage()} - Line: {$e->getLine()}");
            return $this->sendError();
        }
    }

     /**
     * @api {get} books/list Lista de todos los libros/publicaciones en sistema
     * @apiGroup      Libros
     * @apiName       books.list
     *  @apiSuccess    {Boolean} success  indica si todo se ejecuto correctamente
     *  @apiSuccess    {Object[]} data  contiene la informacion los libros
     *  @apiSuccess    {Integer} data.id  id del libro recien creado
     * @apiSuccess      {String} data.title titulo del libro
     *  @apiSuccess      {Text} data.description descripcion del libro
     *  @apiSuccess      {Double} data.price precio del libro
     */
    public function listOfBooks()
    {
        try {
            $books = $this->repository->getAll();
            return $this->sendResponse(BookResource::collection($books), false, Response::HTTP_OK);
        } catch (\Exception $e) {
            Log::info("Ha ocurrido un error, Controller: Api\listOfBooks@index - Error: {$e->getMessage()} - Line: {$e->getLine()}");
            return $this->sendError();
        }
    }

    /**
     * @api {get} books/list/listPaginate Lista filtrada de todos los libros/publicaciones en sistema
     * @apiGroup      Libros
     * @apiName       books.list.paginate
     *  @apiParam      {String="asc","desc"} [sort[direction]=asc] direccion a ordenar
     *  @apiParam      {String} [sort[column]=id] columna a aplicar el orden
     *  @apiParam      {Integer} [perPage=10] cantidad de registros por pagina
     *  @apiParam      {Integer} [page=1] pagina solicitada
     *  @apiSuccess    {Boolean} success  indica si todo se ejecuto correctamente
     *  @apiSuccess    {Object[]} data  contiene la informacion los libros
     *  @apiSuccess    {Integer} data.id  id del libro
     * @apiSuccess      {String} data.title titulo del libro
     *  @apiSuccess      {Text} data.description descripcion del libro
     *  @apiSuccess      {Double} data.price precio del libro
     *  @apiSuccess      {Array} pagination ontiene la informacion del paginado
     *  @apiSuccess      {Integer} pagination.totalPages total de paginas disponibles
     *  @apiSuccess      {Integer} pagination.page pagina actual que esta consultando
     *  @apiSuccess      {Integer} pagination.perPage cantidad de registros por pagina
     *  @apiSuccess      {Integer} pagination.count cantidad de registros en pagina actual
     *  @apiSuccess      {Integer} pagination.from numero de registro desde el cual se esta consultando 
     *  @apiSuccess      {Integer} pagination.to numero de registro hasta el cual se esta consultando 
     *  @apiSuccess      {Integer} pagination.total cantidad total de registros en sistemas
     */
    
    public function listOfBooksPaginate(Request $request)
    {
        try {
            $books = $this->repository->search($request->search, $request->sort, $request->perPage);
            return $this->sendResponse(BookResource::collection($books), false, Response::HTTP_OK, $books);
        } catch (\Exception $e) {
            Log::info("Ha ocurrido un error, Controller: Api\listOfBookPaginate@index - Error: {$e->getMessage()} - Line: {$e->getLine()}");
            return $this->sendError();
        }
    }
}