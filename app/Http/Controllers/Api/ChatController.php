<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use App\Http\Resources\ChatResource;
use App\Repositories\ChatRepository;
use App\Http\Resources\MessageResource;
use App\Http\Controllers\BaseController;

class ChatController extends BaseController
{
    private $repository;

    public function __construct(ChatRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @api {post} api/chats/start Iniciar Chat entre un usuario y el autor del libro
     * @apiGroup      Chats
     * @apiName       chats.start
     *  @apiParam      {Integer} user_id usuario que inicia el chat
     * @apiParam      {Integer} book_id libro implicado
     *  @apiSuccess    {Boolean} success  indica si todo se ejecuto correctamente
     *  @apiSuccess    {Object} data  contiene la informacion del chat
     *  @apiSuccess    {Integer} data.id  id del chat
     * @apiSuccess      {Integer} data.totalMessage total de mensajes en el chat
     *  @apiSuccess      {Date} data.date fecha y hora de creacion
     *  @apiSuccess      {Object} data.user continene la informacion del usuario
     *  @apiSuccess    {Integer} data.user.id  id del usuario
     *  @apiSuccess    {Integer} data.user.name  nombre del usuario
     *  @apiSuccess    {Integer} data.user.email  correo del usuario
      *  @apiSuccess      {Object} data.author continene la informacion del autor del libro
     *  @apiSuccess    {Integer} data.author.id  id del autor
     *  @apiSuccess    {Integer} data.author.name  nombre del autor
     *  @apiSuccess    {Integer} data.author.email  correo del autor
      *  @apiSuccess      {Object} data.book continene la informacion del libro
     *  @apiSuccess    {Integer} data.book.id  id del libro
     * @apiSuccess      {String} data.book.title titulo del libro
     *  @apiSuccess      {Text} data.book.description descripcion del libro
     *  @apiSuccess      {Double} data.book.price precio del libro
     * @apiSuccess    {String} message mensaje alusivo a la acción realizada
     */

    public function start(Request $request)
    {
        try {
            return $this->sendResponse(new ChatResource($this->repository->start($request)), self::START_SUCCESS_MESSAGE, Response::HTTP_CREATED);
        } catch (\Exception $e) {
            Log::info("Ha ocurrido un error, Controller: Api\ChatController@start - Error: {$e->getMessage()} - Line: {$e->getLine()}");
            return $this->sendError();
        }
    }

    /**
     * @api {get} api/chats/get Obtener Chat
     * @apiGroup      Chats
     * @apiName       chats.get
     *  @apiParam      {Integer} chat_id id chat consultado
     *  @apiSuccess    {Boolean} success  indica si todo se ejecuto correctamente
     *  @apiSuccess    {Object} data  contiene la informacion del chat
     *  @apiSuccess    {Integer} data.id  id del chat
     * @apiSuccess      {Integer} data.totalMessage total de mensajes en el chat
     *  @apiSuccess      {Date} data.date fecha y hora de creacion
     *  @apiSuccess      {Object} data.user continene la informacion del usuario
     *  @apiSuccess    {Integer} data.user.id  id del usuario
     *  @apiSuccess    {Integer} data.user.name  nombre del usuario
     *  @apiSuccess    {Integer} data.user.email  correo del usuario
      *  @apiSuccess      {Object} data.author continene la informacion del autor del libro
     *  @apiSuccess    {Integer} data.author.id  id del autor
     *  @apiSuccess    {Integer} data.author.name  nombre del autor
     *  @apiSuccess    {Integer} data.author.email  correo del autor
      *  @apiSuccess      {Object} data.book continene la informacion del libro
     *  @apiSuccess    {Integer} data.book.id  id del libro
     * @apiSuccess      {String} data.book.title titulo del libro
     *  @apiSuccess      {Text} data.book.description descripcion del libro
     *  @apiSuccess      {Double} data.book.price precio del libro
     */

    public function get(Request $request)
    {
        try {
            return $this->sendResponse(new ChatResource($this->repository->findFail($request->chat_id)), false, Response::HTTP_OK);
        } catch (\Exception $e) {
            Log::info("Ha ocurrido un error, Controller: Api\ChatController@get - Error: {$e->getMessage()} - Line: {$e->getLine()}");
            return $this->sendError();
        }
    }

    /**
     * @api {get} chats/messages/get Obtener mensajes de un chat
     * @apiGroup      Chats
     * @apiName       chats.messages.get
     *  @apiParam      {Integer} chat_id id chat consultado
     *  @apiParam      {Integer} [page=1] pagina solicitada
     *  @apiSuccess    {Boolean} success  indica si todo se ejecuto correctamente
     *  @apiSuccess    {Object[]} data  contiene la informacion de los mensajes
     *  @apiSuccess    {Integer} data.id  id del mensaje
     *  @apiSuccess    {Integer} data.status  status del mensaje
     *  @apiSuccess    {Text} data.body  contenido del mensaje
     *  @apiSuccess      {Date} data.date fecha y hora en el que se envio el mensaje
     *  @apiSuccess      {String} data.file url del archivo adjunto
     *  @apiSuccess      {Object} data.sendBy contiene la informacion del usuario que envia el mensaje
     *  @apiSuccess    {Integer} data.sendBy.id  id del usuario que envia el mensaje
     *  @apiSuccess    {Integer} data.sendBy.name  nombre del usuario que envia el mensaje
     *  @apiSuccess    {Integer} data.sendBy.email  correo del usuario que envia el mensaje
     *  @apiSuccess      {Object} data.reciviedBy contiene la informacion del usuario que recibe el mensaje
     *  @apiSuccess    {Integer} data.reciviedBy.id  id del usuario que recibe el mensaje
     *  @apiSuccess    {Integer} data.reciviedBy.name  nombre del usuario que recibe el mensaje
     *  @apiSuccess    {Integer} data.reciviedBy.email  correo del usuario que recibe el mensaje
     */
    public function getMessages(Request $request)
    {
        try {
            $messages = $this->repository->getMessage($request);
            return $this->sendResponse(MessageResource::collection($messages), false, Response::HTTP_OK, $messages);
        } catch (\Exception $e) {
            Log::info("Ha ocurrido un error, Controller: Api\ChatController@storeMessage - Error: {$e->getMessage()} - Line: {$e->getLine()}");
            return $this->sendError();
        }
    }

    /**
     * @api {get} api/chats/messages/send Guardar mensaje
     * @apiGroup      Chats
     * @apiName       chats.messages.store
     *  @apiParam      {Integer} chat_id chat al cual va dirigido el mensaje
     *  @apiParam      {Integer} user_id usuario que envia el mensaje
     *  @apiParam      {Text} body contenido del mensaje
     *  @apiParam      {File} [file] archivo adjunto
     * *  @apiSuccess    {Object} data  contiene la informacion de los mensajes
     *  @apiSuccess    {Integer} data.id  id del mensaje
     *  @apiSuccess    {Integer="1=enviado","2=recibido"} data.status  status del mensaje
     *  @apiSuccess    {Text} data.body  contenido del mensaje
     *  @apiSuccess      {Date} data.date fecha y hora en el que se envio el mensaje
     *  @apiSuccess      {String} data.file url del archivo adjunto
     *  @apiSuccess      {Object} data.sendBy contiene la informacion del usuario que envia el mensaje
     *  @apiSuccess    {Integer} data.sendBy.id  id del usuario que envia el mensaje
     *  @apiSuccess    {Integer} data.sendBy.name  nombre del usuario que envia el mensaje
     *  @apiSuccess    {Integer} data.sendBy.email  correo del usuario que envia el mensaje
     *  @apiSuccess      {Object} data.reciviedBy contiene la informacion del usuario que recibe el mensaje
     *  @apiSuccess    {Integer} data.reciviedBy.id  id del usuario que recibe el mensaje
     *  @apiSuccess    {Integer} data.reciviedBy.name  nombre del usuario que recibe el mensaje
     *  @apiSuccess    {Integer} data.reciviedBy.email  correo del usuario que recibe el mensaje
     */
    public function storeMessage(Request $request)
    {
        try {
            return $this->sendResponse(new MessageResource($this->repository->storeMessage($request)), self::SEND_SUCCESS_MESSAGE, Response::HTTP_CREATED);
        } catch (\Exception $e) {
            Log::info("Ha ocurrido un error, Controller: Api\ChatController@storeMessage - Error: {$e->getMessage()} - Line: {$e->getLine()}");
            return $this->sendError();
        }
    }

}