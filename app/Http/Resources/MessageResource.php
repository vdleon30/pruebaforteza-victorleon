<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MessageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'status' => $this->status,
            'body' => $this->body,
            'date' => $this->created_at,
            'file' => $this->file_name ? asset($this->file_name) :null,
            'sendBy' => new UserResource($this->sendBy),
            'reciviedBy' => new UserResource($this->reciviedBy),
            
        ];
    }
}