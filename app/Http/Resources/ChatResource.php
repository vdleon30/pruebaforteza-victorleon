<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ChatResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'status' => $this->status,
            'totalMessage' => $this->messages->count(),
            'date' => $this->created_at,
            'user' => new UserResource($this->user),
            'author' => new UserResource($this->author),
            'book' => new BookResource($this->book),
        ];
    }
}