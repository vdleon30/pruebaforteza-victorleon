<?php

namespace App\Models;

use App\User;
use App\Models\Book;
use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    protected $fillable = ['user_id', 'author_id', 'book_id', 'status'];
    const PATH_IMG = "assets/files/chats/";
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function author()
    {
        return $this->belongsTo(User::class, 'author_id');
    }

    public function book()
    {
        return $this->belongsTo(Book::class);
    }

    public function messages()
    {
        return $this->hasMany(Message::class)->orderBy("created_at", "desc");
    }

    public function generateFileName($file)
    {
        if (!file_exists(public_path() . self::PATH_IMG . "/{$this->id}")) {
            mkdir(public_path() . self::PATH_IMG . "/{$this->id}", 0777, true);
        }
        return self::PATH_IMG . "{$this->id}/{$this->messages->count()}" . md5(gmdate("Y-m-d H:i:s")) . ".{$file->getClientOriginalExtension()}";
    }

}