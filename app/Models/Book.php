<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Book extends Model
{
    use SoftDeletes;
    protected $fillable = ['user_id', 'title', 'description', 'price'];

    public function getPricePromotionAttribute()
    {
        return $this->activePromotion() ? (($this->price * $this->activePromotion()->percent) / 100) : $this->price;
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function chats()
    {
        return $this->hasMany(Chat::class);
    }

    public function activePromotion()
    {
        return  $this->promotions->where("till" , ">" , gmdate("Y-m-d H:i:s"))->first();
    }


}
