<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    

    protected $fillable = ['recivied_id', 'send_id', 'status', 'chat_id', 'body', 'file_name'];

    public function sendBy()
    {
        return $this->belongsTo(User::class, 'send_id');
    }

    public function reciviedBy()
    {
        return $this->belongsTo(User::class, 'recivied_id');
    }

    public function chat()
    {
        return $this->belongsTo(Chat::class);
    }
}