<?php
namespace App\Interfaces;

use App\Models\Book;
use Illuminate\Http\Request;




interface BookRepositoryInterface 
{
    public function getAll();
    public function getAllByAuthor(int $user_id);
    public function store(Request $request);
    public function modify(Request $data, Book $object);
    public function destroy(Book $object);
    public function findFail($id);
    public function search(string $search, array $sort, int $perPage);

}