<?php
namespace App\Interfaces;
class SortObject
{
    private $column, $direction;
    public function __construct(string $column, string $direction)
    {
        $this->column = $column;
        $this->direction = $direction;
    }
    public function SortObject($column, $direction)
    {
        $this->column = $column;
        $this->direction = $direction;
    }
}