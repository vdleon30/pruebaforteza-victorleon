<?php
namespace App\Interfaces;

use Illuminate\Http\Request;


interface ChatRepositoryInterface 
{
    public function findFail($id);
    public function start(Request $request);
    public function storeMessage(Request $request);
    public function getMessage(Request $request);
}