<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\ChatController;
use App\Http\Controllers\Api\BookController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('books/list', [BookController::class, 'listOfBooks'])->name('books.list');
Route::get('books/listPaginate', [BookController::class, 'listOfBooksPaginate'])->name('books.list.paginate');
Route::apiResource('books', Api\BookController::class)->name("*", "books");
Route::get('chats/get', [ChatController::class, 'get'])->name('chats.get');
Route::post('chats/start', [ChatController::class, 'start'])->name('chats.start');
Route::post('chats/messages/send', [ChatController::class, 'storeMessage'])->name('chats.messages.store');
Route::get('chats/messages/get', [ChatController::class, 'getMessages'])->name('chats.messages.get');
