<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chats', function (Blueprint $table) {
            $table->id();

            $table->integer('status')->default(1);

            $table->foreign('book_id')->references('id')->on('books');
            $table->foreignId('book_id')->nullable();

            $table->foreign('author_id')->references('id')->on('users');
            $table->foreignId('author_id')->nullable();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreignId('user_id')->nullable();

            $table->timestamps();
        });

        Schema::create('messages', function (Blueprint $table) {
            $table->id();
            $table->text('body')->nullable();
            $table->text('file_name')->nullable();
            $table->integer('status')->default(1);

            $table->foreign('chat_id')->references('id')->on('chats');
            $table->foreignId('chat_id')->nullable();

            $table->foreign('send_id')->references('id')->on('users');
            $table->foreignId('send_id')->nullable();

            $table->foreign('recivied_id')->references('id')->on('users');
            $table->foreignId('recivied_id')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages');
        Schema::dropIfExists('chats');
    }
}
